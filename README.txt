BootLoader A.Sc1 2014-2015

Mathieu Calemard Du Gardin - 203697
Daria Kobtseva - 204338
David Robert - 205020
Antoine Simonin - 209745
Corentin Vatan - 205154



N�phi interactive est sp�cialis�e dans les jeux vid�o, adaptation de jeux de soci�t� classiques, pour diverses plates-formes. 
Ils veulent lancer un nouveau dispositif de d�codeur TV comprenant tous leurs hits.



Le dispositif fera marcher une version particuli�re revu � la baisse de Linux. 
N�phi veut que vous cr�ez un bootloader graphique sp�cial qui va charger et ex�cuter le noyau du syst�me tout en affichant un logo de fantaisie sur l��cran.


Vous �tes libre d'utiliser C ou x86 ASM pour votre projet.




Operations 



Le bootloader doit �tre capable de charger et d'ex�cuter un noyau Linux, avec son initrd, si n�cessaire. 
Il ne faut pas laisser l'utilisateur choisir d�autre propri�t�s que les propri�t�s configur�es.

Le bootloader doit montrer une image de fantaisie (�cran splash) sur l'�cran pendant le chargement du noyau. 
Le bootloader devrait �tre en mesure de s�ex�cuter � partir du secteur d'amor�age d'un disque dur (au moins).



Configuration 


Le bootloader devrait �tre configurable. 
Vous pouvez choisir que le programme lise la configuration � partir d'un fichier sur le disque lors de l'ex�cution, ou l�installer le long de la configuration du code ex�cutable sur le secteur d'amor�age. 
La configuration sera alors "en dur" dans le secteur de d�marrage et le chargeur de d�marrage doit �tre �install� � nouveau par un outil d�di� � chaque fois les des changements de configuration.


Les param�tres suivants doivent �tre configurables:

� fichier Kernel
� noyau initrd, le cas �ch�ant.

� image de l'�cran de Splash.

Vous �tes libre de choisir ne importe quel format d'image vous trouvez appropri�.



Rendu 

Les �l�ves doivent rendre les �l�ments suivants:

 
� Une archive zip avec le code source du projet. Le code source doit �galement venir avec le syst�me de construction utilis� (fichier de projet, autotools ...), le cas �ch�ant.


� La documentation du projet, bas� sur le mod�le:
	
La documentation technique expliquant vos choix et / ou mise en �uvre de choix / d�tails 
sur les �l�ments suivants (au moins):
		chargement de fichier (kernel / inird) � partir du disque 
mode graphique
		mode d'emploi (configuration howto)

Le premier document est un document acad�mique. 

Il faut  s�adresser au lecteur comme un enseignant, pas un client. Le dernier devrait s�adresser au lecteur comme un utilisateur. 
Ces documents peuvent �tre en fran�ais ou en anglais, selon votre choix.




Note


Le projet sera �valu� comme suit, sur une �chelle de 26,5 / 22,5:


Documentation (2,5 points)
	Documentation utilisateur (1 point)
	La documentation technique (1,5 points)


Op�ration (10 points)
	Le chargeur de d�marrage montre un �cran d'accueil (4 points)
	Les charges de bootloader et ex�cuter le noyau (4 points)
	
Le chargeur de d�marrage prend en charge Initrds (2 points)


Configuration (10 points)
	La configuration du chargeur de d�marrage est charg� lors de l'ex�cution / cod� en dur dans le secteur d'amor�age (4-6 points)
	
Le chargeur de d�marrage peut �tre configur� (kernel / initrd / splash) (4 points)

Bonus (4 points)